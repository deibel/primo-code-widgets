# Multiple Version of jQuery in Primo  
Primo 4 currently uses jQuery version 1.8.3, which came out in November 2012. Although 
this older version is sufficient for Primo's needs, many JavaScript libraries require
later jQuery versions. For example, the [Bootstrap](http://getbootstrap.com/) library requires 
at least version 1.9.1. In order to use such libraries, we need to be able to install a more 
recent version of jQuery. Unfortunately, we can't simply load a different jQuery version and 
expect everything to work. This would undo any jQuery created events. Primo also appears to rely 
on a few features deprecated from later versions.

This Primo widget shows how to run multiple versions of jQuery in Primo simultaneously. The 
shorthands `$` and `jQuery` will still point to the 1.8.3 version, but you will then be able 
to point other code at the other version.

## Technical Specifications

### Primo Code Widgets Dependencies  
None, save for the other version of jQuery you want to use.

### Browser Compatibility  
Firefox, Chrome, Safari, IE6+ (if not using jQuery v2.*,otherwise IE9+), Edge

### Accessibility  
Not applicable

### Responsiveness  
Not applicable


## Usage  
Configuring Primo to use multiple versions of jQuery is fairly easy, but the trickier part is
making other libraries use the more advanced version.

### Installation  
Copy the code from *multiple-query.html* and paste into your header.html file at the beginning
of the file. It must appear before you load any other JavaScript files.

From that point on, you will be able to refer to the two jQueries as follows:

* `$`, `jQuery`, and `jqPrimo` all refer to version 1.8.3

* `jqLocal` refers to the newer version (1.11.1 in the provided file)

If you wish to use a different version of jQuery, you need to just change the URL in the first
script tag in the provided code.

### Making Other Libraries use jqLocal  
As said earlier, making other libraries use the correct version of jQuery can be tricky. Some
libaries, like Bootstrap, make it easy. Such systems frame their code as follows:

```
+function ($) {
   ...
}(jQuery)
```

This makes `$` a parameter for the code inside the function and is set to the value `jQuery`. 
It is then simple enough to change the above code to:

```
+function ($) {
   ...
}(jqLocal)
```

to get it to use the newer version (jqLocal).

Other libaries may not be so convenient. Changing them could require having to do searches for every 
instance of `$` or `jQuery` in them and switching them to `jqLocal`. Wrapping those libraries in the 
`+function($) {` code may be your best option.

### Usage Notes
Since you will now be managing two versions of jQuery, there a few aspects to keep in mind:

* The two versions of jQuery work in their own universes. An event created in one is not accessible
to the other. Thus, remember that Primo will be using the 1.8.3 version. This means that if you want 
to trigger an element event added by Primo, you will need to use jqPrimo

* Although you can use `$` or `jQuery` to refer to the 1.8.3 version, using `jqPrimo` in your code
makes it more readily clear which one you are using, especially if you have to mix them.

