/* TEXT WIDTH MEASUREMENT IN JQUERY
   ----------------------------------------------------------------------------
   Add a function to jQuery for detecting the width of just the text within an
   element (no padding, margins, borders). 
   
   Warning: Will count hidden text in final width!!
   
   Derived from: 
   http://stackoverflow.com/questions/1582534/calculating-text-width-with-jquery
   
   Last edited 2015.12.17 by Kate Deibel
*/


/* Usage:
   jqLocal(selector).textWidth()
   Returns the width (pixels) that the text within the selected element would 
   take ignoring padding, borders, etc. Only font face, size, style, etc. are 
   considered in this calculation.
   
   If multiple elements match the selector, only the width of the first match 
   will be returned.
   
   Optional Parameters:
   - includeSurroundingSpacing: Boolean (defaults to false)
     If true, any whitespace surrounding the text will be included in the 
     width measurement. Example: <span>  text </span> would have the width of
     'text' plus the two spaces before and the one space afterward.
   
   - copyThisCSS: jQuery object (defaults to undefined)
     If a jQuery object is passed as the second parameter, the CSS of the
     passed object (font-related properties only) eill be used in the 
     calculations instead. 

   Side Effect:
   This code, upon its first run, creates and adds a placeholder <span> element
   to the DOM. This element has hidden visibility, is placed -10000px to the 
   left, and only contains any content for the brief running of a call to the
   function. It should not cause problems with layout nor 
*/   
jqLocal.fn.textWidth = function(includeSurroundingSpacing, copyThisCSS) {
    if(includeSurroundingSpacing === undefined)
        includeSurroundingSpacing = false
    if (!jqLocal.fn.textWidth.fakeEl) {
        jqLocal.fn.textWidth.fakeEl = jqLocal('<span id="textWidthSpan">').appendTo(document.body);
        jqLocal.fn.textWidth.fakeEl.css({ 
            visibility: 'hidden !important',
            position: 'absolute !important',
            left: '-10000px !important',
            top: '0 !important'
        });
        jqLocal.fn.textWidth.fakeEl.attr('aria-hidden','true');
    }            
    
    var $elem = this.first();
    
    var htmlText = $elem.val() || $elem.text();
    htmlText = jqLocal.fn.textWidth.fakeEl.text(htmlText).html(); //encode to HTML
    if(includeSurroundingSpacing) //replace trailing and leading spaces
        htmlText = htmlText.replace(/\s/g, "&nbsp;"); 
    jqLocal.fn.textWidth.fakeEl.html(htmlText);
    
    if(copyThisCSS === undefined) {
        jqLocal.fn.textWidth.fakeEl.css('font-style', $elem.css('font-style'));
        jqLocal.fn.textWidth.fakeEl.css('font-variant', $elem.css('font-variant'));   
        jqLocal.fn.textWidth.fakeEl.css('font-weight', $elem.css('font-weight'));    
        jqLocal.fn.textWidth.fakeEl.css('font-size', $elem.css('font-size'));
        jqLocal.fn.textWidth.fakeEl.css('font-family', $elem.css('font-family'));
    }
    else {
        jqLocal.fn.textWidth.fakeEl.css('font-style', copyThisCSS.css('font-style'));
        jqLocal.fn.textWidth.fakeEl.css('font-variant', copyThisCSS.css('font-variant'));   
        jqLocal.fn.textWidth.fakeEl.css('font-weight', copyThisCSS.css('font-weight'));    
        jqLocal.fn.textWidth.fakeEl.css('font-size', copyThisCSS.css('font-size'));
        jqLocal.fn.textWidth.fakeEl.css('font-family', copyThisCSS.css('font-family'));
    }
    
    var width = jqLocal.fn.textWidth.fakeEl.width();
    jqLocal.fn.textWidth.fakeEl.empty();
    return width;
};

