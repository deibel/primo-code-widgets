# Add a Text Width Function to jQuery  

This JavaScript file adds a function to jQuery that determines the width that the text in
an element will require given a CSS styling. This is a helper utility for laying out 
containers that need to fit their largest content and stay the same size (i.e., a dropdown
menu).

## Technical Specifications  
### Primo Code Widgets Dependencies
Requires the [Multiple Version of jQuery](../MultipleJQuery) widget

### Browser Compatibility  
Firefox, Chrome, Safari, IE8+, Edge

### Accessibility  
Not applicable

### Responsiveness
Not applicable

## Usage  

### Installation  
Be sure to install any required *Primo Code Widget Dependencies* first.

This widget consists of a single JavaScript file *textwidth.js*. This file needs to be linked 
in the *header.html* file AFTER the code for the [MultipleJQuery](../MultipleJQuery) widget. 
This will make the text width code available for other widgets to use.

### Advanced Usage
If you wish to use the TextWidth code in your own scripts, it is fairly straightforward to do
so. Calling the function is the same as other jQuery functions. You just need to be sure to
use the `jqLocal` variable:

```
var width_of_text = jqLocal('selector text here').textWidth();
```

There are two optional parameters that are explained in the JavaScript file. 

### Usage Notes  
* The `textWidth()` function only calculates the width of the text from the FIRST element 
the selector matches with. It is often best to use very specific selectors in order to get
the correct calculations.

* To make its calculations, the method only gets the text from the indicated object. This means
that embedded structure is lost, and so any text hidden (i.e. non-visible
cues for screenreaders) will be included in the width calculations. If a width calculation
seems off, run the `.text()` function on the object to see all of the text considered by the
function.

## Acknowledgements
The code for the `textWidth()` function was inspired by and derived from answers to a
[question on StackOverflow](http://stackoverflow.com/questions/1582534/calculating-text-width-with-jquery).