# Add a Sign-In Banner to Prompt Patrons to Log In  
Patrons do not always recognize that they need to login. One way to better prompt users to login is 
to add a banner to the page with a sign-in prompt.

![Screenshot showing a Sign-In Banner (green bar at bottom)](./example-screenshot.png "Screenshot showing the green Sign-In Banner")

In the above screenshot, the sign-in banner is the green bar that spans the entire page's width. It only appears when the user is
not logged in. It also does not appear on the Guest e-Shelf page as 

## Technical Specifications  

### Primo Code Widgets Dependencies  
None, but both [MutationSupport](../MutationSupport/) and [Glyphicons](../Glyphicons) widgets are recommended.

### Browser Compatibility  
Firefox, Chrome, Safari, IE8+, Edge

### Accessibility  
Does not introduce any additional accessibility issues.

### Responsiveness  
Depending on the length of the message and the width of the device, the sign-in banner may take up a large amount
of display space. Primo's default responsive CSS automatically hides everything in the `'div.EXLSystemFeedback`
element below a certain width, so this won't be an issue on the smallest of displays.

## Usage  

### Installation
The Sign-In banner code consists of a JavaScript file and a CSS file. Both files should be linked to from your
header.html file in some way. Copying the contents of each file into an already existing JavaScript and CSS file
is recommended.

To make the banner display, one of two options must be activated in the *signInBanner.js* file. If you are using
the [MutationSupport](../MutationSupport/) widget, uncomment the `AddMutation` code:

```
AddMutation('div.EXLSystemFeedback', generateSignInBanner);
```

Otherwise, uncomment the `jQuery(document).ready` code:

```
jQuery(document).ready(function() {
   jQuery('div.EXLSystemFeedback').each(function(i,elem) { generateSignInBanner(elem); });
});
```

### Customization  
The text of the message can be edited within the JavaScript code. Specifically, find the lines in the `generateSignInBanner(elem)` 
function that read:

```
/* customize the sign-in text */
var signInText = 'Log in to access UW-restricted materials and services';
```

and edit the text between the two apostrophes.

Further customization can be done via the provided CSS. In particular, if you are using the [Glyphicons](../Glyphicons) widget, there are
two CSS rules to create a `:before` and `:after` pseudoelement that adds a well-styled information icon to the banner (as shown
in the earlier screenshot.

### Usage Notes  

* If there are multiple feedback messages, the sign-in banner will be placed above them. It is advisable to use 
different colors for the sign-in banner than the colors for the other feedback messages. This is why the sign-in 
banner has a specific ID `signInPrompt` for targetting via CSS.

* As seen in the screenshot, the UW has both the normal sign-in link `#exlidSignIn a` and the banner use the same colors to 
indicate their shared functionality.

* The sign-in banner does not appear on the e-shelf page for non-logged in users. This is because there is a different
feedback message already present to indicate that guest e-shelf is only temporary.