# Using Bootstrap Glyphicons in Primo  
[Glyphicon Halflings](http://glyphicons.com) is a font set comprised of various icons that is included
with the [Bootstrap](http://getbootstrap.com/components/#glyphicons) JavaScript library. There are 
several advantages of using font-based icons instead of image files for icons. Not only are the font
icons more readily resizable with less pixelization, a font only requires one web request. 

However, if you're using Bootstrap Glyphicons with Primo, there's a problem. The Primo Back Office does 
not let you upload font files. This means you have to have server space to host your own copy of the 
font files. Or, you can use a CDN for better web performance (and not worry about straining your own
resources).

## Technical Specifications  

### Primo Code Widgets Dependencies
No requirements. The [BetterHiddenCues](../BetterHiddenCues/) is recommended.

### Browser Compatibility  
Firefox, Chrome, Safari, IE8+ 

### Accessibility
Icons should generally be used as decorators only. Buttons, links, etc. should have text that makes
clear what their purpose is. If you are using the glyphicons to convey meaning that is not also 
handled by accompanying text, be sure to provide a hidden cue and apply the `aria-hidden` attribute:

```
<span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
```

### Responsiveness  
Not applicable

## Usage

### Installation  
The CSS file is ready to use as is. You just need to place *glyphicons.css* or its contents some place 
where Primo can access it.

### In HTML  
Once the Glyphicon CSS is available, there are several approaches for adding Glyphicons. The 
[instructions for using Bootstrap Glyphicons](http://getbootstrap.com/components/#glyphicons) are 
perhaps the most straightforward. Simply add a `<span>` tag with the appropriate CSS class:

```
<span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
```

Glyphicons can also be used in `:before` and `:after` CSS rules. For example,

```
#exlidMyAccount a:before {
    content: "\e008";
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;   
    position: relative;
    font-size: 1em;
    font-weight: normal;
    padding-right: 0.2em;
    top: 1px;
    display:inline-block;    
}
```

the above CSS adds the *glyphicon-user* icon to the front of the specified link. `\e008` is the hex code
for that particular character in the Glyphicon font set. To find a particular item's code, find the glyphicon
CSS class name from the [Bootstrap page](http://getbootstrap.com/components/#glyphicons) and then search for
that rule definition in *glyphicons.css*. Note that the above CSS is very similar to the `.glyphicon` class
in *glyphicons.css*.

## Acknowledgements  
The *glyphicons.css* code is only made possible through both the [Bootstrap library](http://getbootstrap.com/) and
the awesome creator of [Glyphicon Halflings](http://glyphicons.com) for sharing them. Please include a link back to
them if possible as a thank you.