/*  Customized Bootstrap download.js file:
    * Dropdown augmented for:
      - Spacebar functionality in Firefox
      - Skips over disabled items
      - Proper handling of spacebar for opening closing
      - Tabbing works among items
      
    Original:
    * Bootstrap v3.3.1 (http://getbootstrap.com)
    * Copyright 2011-2014 Twitter, Inc.
    * Licensed under MIT:
      (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.1
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.1'
  
  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return
    
    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')
    
    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $('<div class="dropdown-backdrop"/>').insertAfter($(this)).on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger('shown.bs.dropdown', relatedTarget)
    }

    return false
  }

  // keyup ignorer added so that spacebar works on Firefox
  Dropdown.prototype.keyup = function(e) {
    e.stopPropagation();
    e.preventDefault();
    return false;      
  }
  
  Dropdown.prototype.keydown = function (e) {
    // added functionality for tab (9)
    if (!/(9|38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return
    
    var $this = $(this)
    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')    
    
    // added: move to the next element if on button, dropdown closed, and tab pressed)
    if (!isActive && e.which == 9 && ! $this.is('.disabled, :disabled') ) 
        return;
    
    e.preventDefault()
    e.stopPropagation()
    
    if ($this.is('.disabled, :disabled')) 
        return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')
    
    // added functionality so that active dropdowns close on space (unless intercepted earlier)
    if ((!isActive && e.which != 27) || (isActive && e.which == 27) || (isActive &&e.which == 32)) {
      if (e.which == 27 || e.which == 32) 
        $parent.find(toggle).trigger('focus')        
      return $this.trigger('click')
    }
    
    // added not(.disabled) so that disabled items never get selected 
    var desc = ' li:not(.divider):not(.disabled):visible '
    var $items = $parent.find('[role="menu"]' + desc + 'a' + ', [role="menu"]' + desc + 'button')
   
    if (!$items.length) return
       
    var index = $items.index(e.target)
    
    // extra booleans added for tabbing
    if ( (e.which == 40 && index < $items.length - 1) || (e.which == 9 && index < $items.length - 1 && !e.shiftKey ) ) index++; // down
    if ( (e.which == 38 && index > 0) || (e.which == 9 && index > 0 && e.shiftKey) ) index--; // up
    if (!~index) index = 0;

    
    $items.eq(index).trigger('focus')
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger('hidden.bs.dropdown', relatedTarget)
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')
      
      if (!data) 
          $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') 
          data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }
  

  
  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  // added events for keyup
  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keyup.dropdown.data-api', toggle, Dropdown.prototype.keyup)    
    .on('keydown.bs.dropdown.data-api', '[role="menu"]', Dropdown.prototype.keydown)
    .on('keyup.bs.dropdown.data-api', '[role="menu"]', Dropdown.prototype.keyup)        
    .on('keydown.bs.dropdown.data-api', '[role="listbox"]', Dropdown.prototype.keydown)

}(jqLocal);


