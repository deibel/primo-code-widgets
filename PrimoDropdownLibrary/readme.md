# Primo Dropdown Library

The default dropdowns in Primo (for search scopes, sorting, and item actions) are not 
accessible via keyboards (and the search scope dropdown is better provided as a <select>
input as by the [DefaultSearchScopeSelect](../DefaultSearchScopeSelect)). This widget 
provides a library for easy generation of 
[Bootstrap dropdowns](http://getbootstrap.com/components/#dropdowns). Other widgets
can utilize the library for simpler code generation.

## Technical Specifications

### Primo Code Widgets Dependencies  
[MultipleJQuery](../MultipleJQuery/) and [TextWidth](../TextWidth) are required.

### Browser Compatibility
Firefox, Chrome, Safari, IE8+, Edge

### Accessibility  
Significantly improves keyboard accessibility.

### Responsiveness
Equivalent to basic Primo responsiveness.

## Usage

### Installation

Be sure to install any required *Primo Code Widget Dependencies* first.

Installing the Primo Dropdown widget involves linking two JavaScript files and a CSS file into your
header.html file:

* A Bootstrap Dropdown Javascript library:  
You can use any version of [Bootstrap's dropdown library](http://getbootstrap.com/components/#dropdowns). 
The file *custome-bootstrap-dropdown.js* is included with this widget. It is a deriviative of version
3.3.1 that incorporates slightly greater keyboard interaction than what Bootstrap provides. 

* *primo-dropdown.js*:  
This file contains the main code of the Primo Dropdown library and automates the process of creating
a Bootstrap-style dropdown.

* *primo-dropdown.css*:  
The CSS for basic styling of a dropdown and its content.

### Creating a Dropdown 

Full examples of using the dropdown library can be seen in other widgets like 
[ReplaceSortsDropdown](../ReplaceSortsDropdown/) and [MoveItemActions](../MoveItemActions/), but 
the following code provides a basic idea of how it works.

First, one must create a new `PrimoDropDown` object and fill in its text and title as below:
```
var dropdown = new PrimoDropDown('sortByMenu');
dropdown.ButtonTitle('Change result sorting');
dropdown.ButtonText('Results sorted by:',
                    'Relevance',
                    'Or Click/Enter to replace sort method');
```

You can then add links or buttons to the dropdown:
```
dropdown.AddLinkItem($('<a id="a1". href="...">Go here</a>'), 'a1');
dropdown.AddButtonItem($('<button id='b1' type="button" onclick="...">Press Me</button>'));
```
Note that you can leave out the second parameter (the id) in most cases as the code will extract
an ID from the passed in jQuery object or generate a generic one.

To make a button or link disabled, just include a third parameter of `true` to indicate it is disabled.
You will have to include the id parameter as well (if only JavaScript had keyword parameters):

```
dropdown.AddButtonItem($('<button id='b2' type="button" onclick="...">NO Press Me</button>'), 'b2', true);
```

To place the dropdown on the page, you need to use the `GetMenu()` method:
```
$('somewhere on the page').append( dropdown.GetMenu() );
```

One the dropdown has been placed, it is smart to run the `FitDropDown()` method to ensure that the 
widths of the menu are properly sized. If the text of the dropdown button needs to change 
(as in [ReplaceSortsDropdown](../ReplaceSortsDropdown/)), you should run the function with `true`
as the parameter (otherwise leave blank as it defaults to `false`):
```
dropdown.FitDropDown();
```

### Customization

The provided CSS file *primo-dropdown.css* is where some aspects of the dropdown can be tweaked. Some
basic manipulations are described below.

The colors for when the button is hovered on or focused is controlled by the selectors 
`.local-dropdown-button:focus, .local-dropdown-button:hover`.

To change the color of the dropdown button caret, find the selector `.local-dropdown-button:after` and
change the color in the rule `border-top: 8px solid #4b2e83;`. The caret is drawn using borders to 
simulate a triangle shape. If you change the caret's color, you may also want to adjust its colors
when the button is hovered or focused on via the selectors `.local-dropdown-button:focus:after,
.local-dropdown-button:hover:after`.

By default, the dropdown button and the menu that appears are flush left; the left edges of both line
up with each other. If you want the menu to be flush right with the button, add the following CSS 
(replace *#DROPDOWN_ID* with the ID that you gave the dropdown when you created it:
```
#DROPDOWN_ID ul.dropdown-menu {
   left: auto;
   right: 0;
}
```

##  Acknowledgements  
The *custom-bootstrap-dropdown.js* code is only made possible through the [Bootstrap library](http://getbootstrap.com/)
for sharing them. Please include a link back to them if possible as a thank you.









