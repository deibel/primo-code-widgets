/* LIBRARY FOR EASY GENERATION OF BOOTSTRAP DROPDOWN MENUS
   -----------------------------------------------------------------
   This library provides automation for generating Bootstrap-style
   dropdown menus for use in Primo. Menus can contain either buttons
   or links. Functions are provided for properly sizing the width of
   the dropdown.
   
   Last edited 2016.02.04 by Kate Deibel
*/

/* Constructor that creates a PrimoDropDown object with the designated ID */
function PrimoDropDown(_id) {
    this.id = _id;
    this.dropdown = null;

    this.generate = function() {
        this.dropdown = jqLocal('<div class="dropdown">').attr('id', this.id);
        this.dropdown.append('<button class="local-dropdown-button" type="button">');
        this.dropdown.find('button').attr({
            id: this.id + "-button",
            title: "Drop down menu",
            "data-toggle": "dropdown",
            "aria-haspopup": "true",
            "aria-expanded": "false"
        });
        this.dropdown.append('<ul class="dropdown-menu" role="menu">');
        this.dropdown.find('ul').attr('aria-labelledby', this.id + '-button');
        
    };            
        
    this.generate();
}

/* Set the title/hover text of the dropdown's button' */
PrimoDropDown.prototype.ButtonTitle = function(txt) {
    this.dropdown.find('button').attr('title', txt);
}

/* Set the text that appears in the button:
   Parameters:
      prePrompt     Hidden text presented only to screenreaders (before text)
      text          The visible text in the button
      postPrompt    Hidden text presented only to screenreaders (after text)
*/      
PrimoDropDown.prototype.ButtonText = function(prePrompt,text,postPrompt) {
    if(prePrompt === undefined) prePrompt = "";
    if(text === undefined) text = "";
    if(postPrompt === undefined) postPrompt = "";
    
    this.dropdown.find('button').empty();
    
    var pre = prePrompt.trim();
    if(pre.length > 0)
        this.dropdown.find('button').append('<span class="EXLHiddenCue">' + pre + ' </span>');
    this.dropdown.find('button').append('<span class="buttonText ieButtonFix">' + text.trim() + '</span>');
    var post = postPrompt.trim();
    if(post.length > 0)
        this.dropdown.find('button').append('<span class="EXLHiddenCue"> ' + post + '</span>');        
}

/* Add a <a> item to the dropdown:
   Parameters:
      a_jq      A jquery object of the <a> to be added
      a_id      The ID that is to be assigned to the link. If parameter is
                left missing, a numbered ID is automatically assigned.
      disabled  Boolean indicating if the link should be disabled
*/
PrimoDropDown.prototype.AddLinkItem = function(a_jq, a_id, disabled) {
    if(a_id === undefined) a_id =  a_jq.attr('id');
    if(a_id === undefined) a_id = this.id + '-link-' + (this.dropdown.find('a').length + 1);
    if(disabled === undefined) disabled = false;

    var opt = jqLocal('<li role="presentation">');
    opt.append(a_jq);
    opt.find('a').attr({
        role: "menuitem",
        tabindex: "0",
        id: a_id
    });
    if(disabled) {
        opt.attr('tabindex', -1);
        opt.addClass('disabled');
    }
    
    this.dropdown.find('ul').append(opt);
}

/* Add a <button> item to the dropdown:
   Parameters:
      a_jq      A jquery object of the <button> to be added
      a_id      The ID that is to be assigned to the button. If parameter is
                left missing, a numbered ID is automatically assigned.
      disabled  Boolean indicating if the button should be disabled
*/
PrimoDropDown.prototype.AddButtonItem = function(b_jq, b_id, disabled) {
    if(b_id === undefined) b_id =  b_jq.attr('id');
    if(b_id === undefined) b_id = this.id + '-button-' + (this.dropdown.find('button').length + 1);
    if(disabled === undefined) disabled = false;

    var opt = jqLocal('<li role="presentation">');
    opt.append(b_jq);
    opt.find('button').attr({
        role: "menuitem",
        tabindex: "0",
        id: b_id
    });
    if(disabled) {
        opt.attr('tabindex', -1);
        opt.addClass('disabled');
    }
    
    b_jq.on( 'keydown', function(event) {
        var $this = jqLocal(this);
        if(event.which == 32 && ! $this.is('.disabled, :disabled') ) {
            event.preventDefault();
            event.stopPropagation();
            return $this.trigger('click');
        }
    });
    
    this.dropdown.find('ul').append(opt);
}

/* Returns the jQuery object that contains the generated dropdown menu */
PrimoDropDown.prototype.GetMenu = function() {
    return this.dropdown;
}

/* Determines the text width and LR padding/border for the dropdown 
   button and each item in the list. 
   
   if testAllTextInDropdown, additional widths are calculated as if 
   any of the item's text could be placed in the local-dropdown-button 
   (as with the Sort By dropdown)
   
   A min-width CSS attribute is applied to either the entire dropdown
   (when testAllTextInDropdown is true) or to just the menu (ul).
*/
PrimoDropDown.prototype.FitDropDown = function(testAllTextInDropdown) {

    if(testAllTextInDropdown === undefined) testAllTextInDropdown = false;

    var $dropdown = this.dropdown;      

    var maxWidth = 0;  

    if(testAllTextInDropdown) {
        var $local = $dropdown.find('.local-dropdown-button');
        var localDelta = 0;
        localDelta += parseInt($local.css('border-left-width'), 10);
        localDelta += parseInt($local.css('padding-left'), 10);
        localDelta += parseInt($local.css('padding-right'), 10);    
        localDelta += parseInt($local.css('border-right-width'), 10);
        
        $dropdown.find('ul a').each(function(index, item) { 
            maxWidth = Math.max(maxWidth, jqLocal(item).textWidth(false,$local));       
        });
        $dropdown.find('ul button .buttonText').each(function(index, item) {
            maxWidth = Math.max(maxWidth, jqLocal(item).textWidth(false,$local));
        });
        
        maxWidth += localDelta;
    }

    $dropdown.find('ul a').each(function(index, item) { 
        var $item = jqLocal(item); 
        var textWidth = $item.textWidth();
        
        var itemDelta = 0;
        itemDelta += parseInt($item.css('border-left-width'), 10);   
        itemDelta += parseInt($item.css('padding-left'), 10);        
        itemDelta += parseInt($item.css('padding-right'), 10);       
        itemDelta += parseInt($item.css('border-right-width'), 10);         
        
        maxWidth = Math.max(maxWidth, textWidth + itemDelta); 
    });
    
    $dropdown.find('ul button').each(function(index, item) { 
        var $item = jqLocal(item); 
        var textWidth = $item.find('.buttonText').textWidth();
        
        var itemDelta = 0;
        itemDelta += parseInt($item.css('border-left-width'), 10);   
        itemDelta += parseInt($item.css('padding-left'), 10);        
        itemDelta += parseInt($item.css('padding-right'), 10);     
        itemDelta += parseInt($item.css('border-right-width'), 10);       

        maxWidth = Math.max(maxWidth, textWidth + itemDelta);   
    });    
    
    if(testAllTextInDropdown) {
        /* set the min-width to the whole dropdown */
        $dropdown.css('min-width', maxWidth + 'px');
    }
    else {
        /* set the min-width to just the menu (ul) */
        $dropdown.find('ul').css('min-width', maxWidth + 'px');
    }
}