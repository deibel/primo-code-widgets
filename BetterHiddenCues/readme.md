# Better Hidden Cues for Screen Readers

Primo already has two CSS classes, `.EXLHiddenCue` and `.EXLHide`, that provide a means of 
hiding text so that it can be seen by screen readers but is otherwise invisible and 
unobtrusive to sighted users. The [Bootstrap](http://getbootstrap.com/) library similarly
uses the `.sr-only` CSS class. All three CSS classes use or more the 
[standardized approaches](http://webaim.org/techniques/css/invisiblecontent/) for hiding text:
positioning it absolutely to the negative left of the screen, hiding overflow, setting a tiny 
width, and clipping the content. This widget just brings them all into consistency and using 
the recommended techniques.

## Technical Specifications  

### Primo Code Widgets Dependencies
None.

### Browser Compatibility  
Firefox, Chrome, Safari, IE6+ 

### Accessibility
Improves accessibility when used.

### Responsiveness
Not applicable.

## Usage

### Installation
The CSS file is ready to use as is. You just need to place *hidden-cues.css* or its contents
somewhere that Primo can access it.

### In HTML
To provide cues for screenreaders, one just needs to apply one of the CSS classes to an HTML
element. For example, the default sorting menu in Primo's Brief Results has the following HTML:

```
<span class="EXLResultsSortBySelected">
   <a href="#" title="Sort">
      <span class="EXLHiddenCue">Sorted by:</span>
      Relevance
      <span class="EXLHiddenCue">Or hit Enter to replace sort method</span>
   </a> 
</span>
```

This link both states how the results are currently sorted but is also the prompt to choose a 
different sort option. If it only contained the text 'Relevance,' a screen reader user would 
not know what the link does (the title attribute does give a slight suggestion of something 
regarding sorting but does not describe the interaction at all).

However, the hidden cues tells the screen reader something more: "Sorted by: Relevance or hit 
Enter to replace sort." A sighted user knows that a dropdown will appear due to the down chevron
next to the link. The hidden cue conveys that some sort of interaction occurs with the link 
(which really should be a button, but that's a different discussion).

 