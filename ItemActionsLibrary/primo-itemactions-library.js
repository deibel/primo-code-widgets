/* PRIMO ITEM ACTIONS LIBRARY
   ----------------------------------------------------------------------------
   A JavaScript library for generating various commands that can be applied to
   search results in Primo: 
   - Permalink
   - Print
   - E-Mail
   - e-Shelf
   - Send to EndNote
   - Send to EasyBib
   - Send to RefWorks
   - Export RIS
   - Save search query
   - Add all items on page to e-Shelf   

   Code is customizable and can be used to create new item actions.
   
   Last edited 2016.02.18 by Kate Deibel
*/


/* Constructor to create a PrimoItemTools instance
   Parameter:
       _record: a jQuery object that has the .EXLResult CSS class
*/
function PrimoItemTools(_record) {
    this.record = _record;
    this.baseItem = null;
    this.indexOnPage = 0;
    this.indexOverall = 0;    

    this.scopes = jQuery('#scopesListContainer').find('input:checked').val();

    this.recordID = this.record.find('.EXLResultRecordId').attr('id');
    if(this.recordID === undefined)
        this.recordID = jQuery('.EXLResultRecordId').attr('id');

    if( jQuery('#vid').length === 0)  /* browse search uses a different request/view format */
        this.vid = jQuery('#vid_browse_input').val();
    else
        this.vid = jQuery('#vid').val();

    this.tab = jQuery('#tab').val();
    if(this.tab)
        this.tab = "&amp;tab=" + this.tab;

    /* attempt to grab the indexOnPage from the _record object's id, 
       otherwise default to 0 */
    if( _record.attr('id') === undefined ) {
        this.indexOnPage = 0;
    }
    else {
        this.indexOnPage = parseInt(_record.attr('id').replace('exlidResult',''));
    }
    /* #indx contains the starting index of the first item on the page 
       (based on results per page) */
    if( jQuery('#indx').val() ) 
        this.indexOverall = this.indexOnPage + parseInt( jQuery('#indx').val() );
    else 
        this.indexOverall = this.indexOnPage + 1;  

    this.baseItem = PrimoItemTools.GenerateBase();
}

/* Static function to create a generic jQuery button object */
PrimoItemTools.GenerateBase = function() {
    var baseItem = jQuery('<button type="button" class="localActionButton" tabindex="0">');
    baseItem.append('<span class="buttonText"></span>');
    baseItem.append('<span class="glyphicon" aria-hidden="true"></span>');
    return baseItem;
}

/* Static function that creates a jQuery button object for adding
   all items on a page to the e-Shelf */
PrimoItemTools.AddPageToEShelf = function() {
    tool = PrimoItemTools.GenerateBase();
    tool.addClass('pageEshelf_ActionButton');
    /* add a class with index so that we can easily access all of them */
    tool.attr('id', 'pageEShelfButton');
    tool.attr('name', 'pageEShelfButton');
    tool.find('title').html('Add Page to e-Shelf');
    tool.find('.buttonText').html('Add Page to e-Shelf');
    tool.find('.glyphicon').addClass('glyphicon-tags');

    tool.on( 'click', function(event) {
        var $this = jQuery(this);
        var $stars = jQuery('.eshelf_ActionButton.off_shelf');
        event.preventDefault();

        $stars.toggleClass('on_shelf off_shelf');

        jqPrimo('.EXLFacetSaveToEShelfAction a').trigger('click');
    });

    return tool;
}

/* Static function that creates a jQuery button object for saving the
   search query on a non-full display page */
PrimoItemTools.SaveQuery = function() {
    tool = PrimoItemTools.GenerateBase();
    tool.addClass('pageEshelf_ActionButton');
    /* add a class with index so that we can easily access all of them */
    tool.attr('id', 'saveQueryButton');
    tool.attr('name', 'saveQueryButton');
    tool.attr('title', 'Save Current Search (opens in a new window)');
    tool.find('.buttonText').html('Save Current Search');
    tool.find('.glyphicon').addClass('glyphicon-bookmark');

    tool.on( 'click', function(event) {
        jqPrimo('.EXLFacetSaveSearchAction a').trigger('click');
    });

    return tool;
}

/* Generate an e-shelf jQuery button for the current item. This button
   handles both adding and removing the item the e-Shelf. The icons/hovertext
   auto-adjust to indicate either case. */
PrimoItemTools.prototype.eShelf = function() {
    tool = this.baseItem.clone();
    tool.addClass('eshelf_ActionButton');
    /* add a class with index so that we can easily access all of them */
    tool.addClass('eshelf-button-class-' + this.indexOnPage);

    tool.attr('id', 'eshelf-record-' + this.indexOnPage);
    tool.attr('name', 'eshelf-record-' + this.indexOnPage);
    tool.find('.buttonText').html('e-Shelf');

    tool.find('.glyphicon').clone().appendTo(tool);
    tool.find('.glyphicon:even').addClass('glyphicon-star');
    tool.find('.glyphicon:odd').addClass('glyphicon-star-empty');

    /* determine if it is supposed to be on or off indicator */
    if(window.isFullDisplay()) {
        if(jQuery('.EXLTabHeaderButtonSendToList .EXLButtonSendToMyShelfAdd').css('display')  == 'block') {
            tool.addClass('off_shelf');
            tool.attr('title', 'Add record to e-Shelf');
        }
        else {
            tool.addClass('on_shelf');
            tool.attr('title', 'Remove record from e-Shelf');
        }
    }
    // check brief result
    else if(String(this.record.find('td.EXLMyShelfStar a img').attr('src')).lastIndexOf('off.png') > -1 ) {
        tool.addClass('off_shelf');
        tool.attr('title', 'Add record to e-Shelf');
    }
    else {
        tool.addClass('on_shelf');
        tool.attr('title', 'Remove record from e-Shelf');
    }

    tool.on( 'click', {recordID: this.recordID, scopes: this.scopes, index: this.indexOnPage}, function(event) {
        var recordID = event.data.recordID;
        var scopes = event.data.scopes;
        var index = event.data.index;
        var $this = jQuery(this);
        var $stars = jQuery('.eshelf-button-class-'+index);
        event.preventDefault();

        var ret;

        if(jQuery(this).hasClass('off_shelf')) {
            ret = eshelfCreate(this, recordID, 'false', scopes, "" + index);
            $stars.attr('title', 'Remove record from e-Shelf');
        }
        else {
            ret = eshelfRemove(this, recordID, 'false', scopes, "" + index);
            $stars.attr('title', 'Add record to e-Shelf');
        }
        /* fix other e-shelf buttons so that they are consistent */
        $stars.toggleClass('on_shelf off_shelf');

        if($this.parents('.dropdown').length > 0) {
            $this.parents('.dropdown').find('.local-dropdown-button').trigger('click');
        }

        return ret;
    });

    return tool;
}

/* Generate a print jQuery button for the current item */
PrimoItemTools.prototype.Print = function() {
    tool = this.baseItem.clone();
    tool.addClass('print_ActionButton');
    tool.attr('id', 'print-record-' + this.indexOnPage);
    tool.attr('name', 'print-record-' + this.indexOnPage);
    tool.attr('title', 'Print record (opens in a new window)');
    tool.find('.buttonText').html('Print');
    tool.find('.glyphicon').addClass('glyphicon-print');

    printLink = jQuery('<a href='
        + ( addSessionId('display.do') )
        + '?fn=print' + this.tab
        + '&indx=' + this.indexOnPage
        + '&display=print&docs=' + this.recordID
        + '&" target="_blank">NOT SEEN</a>');
    printLink.css('display','none');

    tool.on( 'click', { printLink: printLink[0] }, function(event) {
        event.preventDefault();
        return sendPrintPopOut(event.data.printLink);
    });

    return tool;
}

/* Generate an e-mail jQuery button for the current item */
PrimoItemTools.prototype.Email = function() {
    tool = this.baseItem.clone();
    tool.addClass('email_ActionButton');
    tool.attr('id', 'email-record-' + this.indexOnPage);
    tool.attr('name', 'email-record-' + this.indexOnPage);
    tool.attr('title', 'Send record by E-mail (opens in a new window)');
    tool.find('.buttonText').html('E-mail');
    tool.find('.glyphicon').addClass('glyphicon-send');

    mailLink = jQuery('<a href='
        + ( addSessionId('email.do') )
        + '?fn=email&docs=' + this.recordID
        + '&vid=' + this.vid
        + '&fromCommand=true&doc=' + this.recordID
        + '&scope=' + ( escape(this.scopes) )
        + '&indx=' + this.indexOnPage
        + '&" target="_blank">NOT SEEN</a>');

    tool.on( 'click', { mailLink: mailLink[0] }, function(event) {
        event.preventDefault();
        return sendPrintPopOut(event.data.mailLink);
    });

    return tool;
}

/* Generate a permalink jQuery button for the current item
   NOTE: The type of permalink generated depends on if the
         page view is a services page or not.
*/
PrimoItemTools.prototype.Permalink = function() {
    tool = this.baseItem.clone();
    tool.addClass('permalink_ActionButton');
    tool.attr('id', 'permalink-record-' + this.indexOnPage);
    tool.attr('name', 'permalink-record-' + this.indexOnPage);
    tool.attr('title', 'Permalink URL for this record');
    tool.find('.buttonText').html('Permalink');
    tool.find('.glyphicon').addClass('glyphicon-link');

    if(onServicesPage()) { /* use open url permalink generator */
        tool.on( 'click', {recordID: this.recordID, index: this.indexOnPage}, function(event) {
            var recordID = event.data.recordID;
            var index = event.data.index;
            event.preventDefault();
            return openUrlLbox('0', recordID);
        });
    }
    else { /* use regular permalink generator */
        tool.on( 'click', {vid: this.vid, recordID: this.recordID, index: this.indexOnPage}, function(event) {
            var vid = event.data.vid;
            var recordID = event.data.recordID;
            var index = event.data.index;
            event.preventDefault();
            return openPermaLinkLbox('permalink', "docId=" + recordID + "&amp;vid=" + vid + "&amp;fn=permalink", "" + (index - 1), recordID);
        });
    }

    return tool;
}

/* Generate a citation jQuery button for the current item */
PrimoItemTools.prototype.Citation = function() {
    tool = this.baseItem.clone();
    tool.addClass('citation_ActionButton');
    tool.attr('id', 'citation-record-' + this.indexOnPage);
    tool.attr('name', 'citation-record-' + this.indexOnPage);
    tool.attr('title', 'Bibliographic citation for this record');
    tool.find('.buttonText').html('Citation');
    tool.find('.glyphicon').addClass('glyphicon-pencil');

    tool.on( 'click', {recordID: this.recordID, index: this.indexOnPage}, function(event) {
        var recordID = event.data.recordID;
        var index = event.data.index;
        event.preventDefault();
        return openCitationLbox("" + (index - 1), recordID);
    });

    return tool;
}

/* Generate a push to endnote jQuery button for the current item */
PrimoItemTools.prototype.EndNote = function() {
    tool = this.baseItem.clone();
    tool.addClass('endnote_ActionButton');
    tool.attr('id', 'endnote-record-' + this.indexOverall);
    tool.attr('name', 'endnote-record-' + this.indexOverall);
    tool.attr('title', 'Upload bibliographic record to EndNote Basic (online service)');
    tool.find('.buttonText').html('EndNote');
    tool.find('.glyphicon').addClass('glyphicon-cloud-upload');
    /* Uses the overall index */
    tool.on( 'click', {recordID: this.recordID, index: this.indexOverall}, function(event) {
        var recordID = event.data.recordID;
        var index = event.data.index;
        event.preventDefault();
        return pushto('EndNote', "" + index, 'false', recordID);
    });

    return tool;
}

/* Generate a push to RefWorks jQuery button for the current item */
PrimoItemTools.prototype.RefWorks = function() {
    tool = this.baseItem.clone();
    tool.addClass('refworks_ActionButton');
    tool.attr('id', 'refworks-record-' + this.indexOverall);
    tool.attr('name', 'refworks-record-' + this.indexOverall);
    tool.attr('title', 'Upload bibliographic record to RefWorks (online service)');
    tool.find('.buttonText').html('RefWorks');
    tool.find('.glyphicon').addClass('glyphicon-cloud-upload');
    /* Uses the overall index */
    tool.on( 'click', {recordID: this.recordID, index: this.indexOverall}, function(event) {
        var recordID = event.data.recordID;
        var index = event.data.index;
        event.preventDefault();
        return pushto('RefWorks', "" + index, 'false', recordID);
    });

    return tool;
}

/* Generate an export to RIS jQuery button for the current item */
PrimoItemTools.prototype.ExportRIS = function() {
    tool = this.baseItem.clone();
    tool.addClass('exportRIS_ActionButton');
    tool.attr('id', 'exportRIS-record-' + this.indexOverall);
    tool.attr('name', 'exportRIS-record-' + this.indexOverall);
    tool.attr('title', 'Export/save the bibliographic record');
    tool.find('.buttonText').html('Export Record (RIS)');
    tool.find('.glyphicon').addClass('glyphicon-floppy-disk');
    /* Uses the overall index */
    tool.on( 'click', {recordID: this.recordID, index: this.indexOverall}, function(event) {
        var recordID = event.data.recordID;
        var index = event.data.index;
        event.preventDefault();
        return pushto('RISPushTo', "" + index, 'false', recordID);
    });

    return tool;
}

/* Generate a push to EasyBib jQuery button for the current item */
PrimoItemTools.prototype.EasyBib = function() {
    tool = this.baseItem.clone();
    tool.addClass('easybib_ActionButton');
    tool.attr('id', 'easybib-record-' + this.indexOverall);
    tool.attr('name', 'easybib-record-' + this.indexOverall);
    tool.attr('title', 'Upload record to EasyBib');
    tool.find('.buttonText').html('EasyBib');
    tool.find('.glyphicon').addClass('glyphicon-cloud-upload');
    /* Uses the overall index */
    tool.on( 'click', {recordID: this.recordID, index: this.indexOverall}, function(event) {
        var recordID = event.data.recordID;
        var index = event.data.index;
        event.preventDefault();
        return pushto('EasyBib', "" + index, 'false', recordID);
    });

    return tool;
}
