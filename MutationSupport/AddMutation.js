/* AddMutation
   ----------------------------------------------------------------------------
   Support code for using DOM_WAITER.
   
   Last edited 2015.12.14 by Kate Deibel
 */

/* set a variable to the jQuery you want to use */
var AddMutation_default_jquery = jQuery;

/* Wrapper for DOM_WAITER.watch that includes a shiv in case
   the browser does not support Mutation Observers.
     selector:    CSS-style selector the element(s) to be targeted
     fn:          Function to be called on the element(s) matchign selector
     autoDelete:  Whether or not the selector should be removed from the 
                  dom_waiter after it is detected for the first time.
                  Defaults to true.
     JQ:          The name for the jQuery that the shiv will use. 
                  Defaults to the value in AddMutation_default_jQuery
*/     
function AddMutation(selector, fn, autoDelete, JQ) {
    /* by default, we auto delete */
    if(autoDelete === undefined) autoDelete = true;
    /* use the constant for the jQuery if not defined */
    if(JQ === undefined) JQ = AddMutation_default_jquery;
    if(DOM_WAITER.SUPPORTED) {
        /* mutation observers allowed */
        var mutateFn = fn;
        if(autoDelete) {
            mutateFn = function(elem) {
                DOM_WAITER.remove(selector);
                fn.call(elem,elem);
            }
        }
        DOM_WAITER.watch(selector, mutateFn);           
    }
    else {
        JQ(document).ready(function() {
            JQ(selector).each(function(i,elem) { fn.call(elem,elem); });
        });   
    }
} 