/* DOM_WAITER
   ----------------------------------------------------------------------------
   Uses mutation observer to wait for when certain selectors are added to a page
   (ideally during the parsing process). Based in part on the ready.js code from
   ryanmorr (https:/*github.com/ryanmorr/ready)
   
   Last edited 2015.12.14 by Kate Deibel
 */

var DOM_WAITER = {
    watchlist: [], /* list of (selector,function) pairs to keep watch for */
    removelist: [], /* associative array of selectors to remove at next opportunity
                       if marked for removal, the selector will be ignored */
    MutationObserver: window.MutationObserver || window.WebKitMutationObserver,
    watcher: undefined   
}    

/* Detect if DOM_WAITER will work due to the browser supporting Mutation Observer */
DOM_WAITER.SUPPORTED = 'MutationObserver' in window;

/* Function called by the Mutation Observer upon a Mutation Event */
DOM_WAITER.scan = function() {
    var newList = []; /* store the selectors we want to keep after this scan */
    /* scan the entire document for any selectors of interest */
    for(var i=0, ilen=DOM_WAITER.watchlist.length; i<ilen; i++) {
        var selector = DOM_WAITER.watchlist[i].selector;
        var fn = DOM_WAITER.watchlist[i].fn;
        
        /* check if the selector is to be ignored (marked for removal) */
        if (DOM_WAITER.removelist[ selector ] !== undefined) {
            /* This selector was marked as removed, so skip over it. 
               Note: selector will be removed the watch list as well */
            delete DOM_WAITER.removelist[ selector ];
            continue;       
        }     

        /* Grab DOM elements matching watchlist[i]'s selector */
        var elements = window.document.querySelectorAll(selector);
        for(var j=0, jlen=elements.length; j<jlen; j++) {
            var el = elements[j];
            /* Set lastWaitedOnBy so that the DOM addition for the element is noted only once.
               We use the selector as the value so that multiple objects 
            */
            if(el.lastWaitedOnBy === undefined || el.lastWaitedOnBy != selector) {
                el.lastWaitedOnBy = selector;
                /* Invoke the associated callback function */
                fn.call(el, el);
            }
        }
        
        if (DOM_WAITER.removelist[ selector ] === undefined) {
            /* keep this selector */
            newList.push({
                selector: selector,
                fn: fn
            });                
        }
        else {
            /* the remove selector is not needed anymore */
            delete DOM_WAITER.removelist[ selector ];            
        }        
    }
    
    /* swap the old watchlist for the new one */
    DOM_WAITER.watchlist = newList;

    /* shutdown the Mutation Observer if there is no longer a watchlist */
    if(DOM_WAITER.watchlist.length == 0)
        DOM_WAITER.shutdown();        
}

/* Store the selector to be monitored and the associated function callback.
   Turn on the Mutation Observer if necessary.
   The function should ideally be of the form function(elem) where elem is the
   DOM object that DOM_WAITER has detected (i.e., matches the selector).
*/
DOM_WAITER.watch = function(selector, fn) {
    this.watchlist.push({
        selector: selector,
        fn: fn
    });
    if(!this.sentry){
        /* Watch for changes in the document */
        this.sentry = new MutationObserver(this.scan);
        this.sentry.observe(window.document.documentElement, { /* element is the whole page document */
            childList: true, /* observe mutations on child elements */
            subtree: true /* observe mutations on both the target and descendants */
        });
    }
    /* Just in case, check to see if the element is already in the DOM */
    this.scan();
}

/* Marks a selector for removal. The selector will not be removed until the
   next scan occurs. 
*/   
DOM_WAITER.remove = function(selector) {
    /* Due to concurrency issues, we just add the selector to an ignore list */
    DOM_WAITER.removelist[selector] = true    
}

/* Closes the Mutation Observer (sentry) and resets the various lists. */
DOM_WAITER.shutdown = function() {
    if(DOM_WAITER.sentry !== undefined) {
        DOM_WAITER.sentry.disconnect();
        DOM_WAITER.sentry = undefined; 
    }
    DOM_WAITER.watchlist = []
    DOM_WAITER.removelist = []
}
