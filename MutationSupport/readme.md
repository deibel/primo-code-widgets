# Mutation Support  
Mutation Support is a small JavaScript library that uses 
[DOM mutation observers](https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver) to 
improve the speed and ease of making changes to Primo. Instead of waiting for the page to load,
changes to a page element can be made as soon as the element is parsed and before it is rendered. 
This eliminates the page from flashing as rendered elements are added, deleted, or changed. 

## Technical Specifications

## Primo Code Widgets Dependencies  
Mutation Support is independent of the other widgets and therefore has no requirements.
However, we do recommend using [Multiple jQuery Versions in Primo](../MultipleJQuery) widget in order to have the most
secure/complete version of jQuery running.

## Browser Compatibility

All browsers. AddMutation will defer to jQuery `document.ready()` if Mutation Events are not supported
(primarily IE<11).

## Accessibiltiy
Improves screen reader performance by lessening page re-rendering.

## Responsiveness
Not applicable.

## Usage   
Using this library requires the inclusion of the two JS files in this directory. These should be loaded in
your header HTML file.

1. dom_waiter.js  
This is the primary code that handles the mutation observing. How it essentially works 
(see the *How It Works* section below for details) is by using the `watch(selector,fn)` method, where
`selector` is a CSS selector for the element of interest, and `fn` is the callback function that you want applied
to an element that matches the selector. You can add as many `(selector,fn)` pairings as you want.
As new page elements are added, the DOM_WAITER code will keep a lookout for any newly added elements that 
match the set of selectors. If a match is found, it will apply the paired function exactly once to that
element.

2. AddMutation.js  
This file contains a helper function that simplifies the use of the DOM_WAITER. The `AddMudation(selector, fn, autoDelete, JQ)`
function is a wrapper for the and includes a shiv to allow
for [older browser support] (http://caniuse.com/#feat=mutationobserver)(mainly <IE11). This shiv does not 
replicate mutation observers in older browsers and instead defers everything to jQuery's `document.ready()`
function. One can set which jquery the document.ready event gets called from as well. It is recommended that
one uses the [MultipleJQuery](../MultipleJQuery) widget widget and set `AddMutation_default_jquery = jqLocal;`

### Installation  
1. Make copies of both dom_waiter.js and AddMutation.js. 

2. If using the *Multiple jQuery Versions in Primo*, edit line 8 in AddMutation.js to
   read `var AddMutation_default_jquery = jqLocal;`. 
   
3. Place the JavaScript files where you will want Primo to access
them (uploading to the Primo server is perhaps most efficient).

4. In your header.html file, link to both the JS files. 

5. In your header.html file, use *AddMutation* to make changes to he pages.


### Usage Notes  
Using Mutation Support does involve understanding some rules and design limitations of the above code.

* Most other Primo Code Widgets will provide the recommended code for using the AddMutation function. Use it.

* Less is more when it comes to using mutations. The only reason we are using it with Primo 4 is due to the 
limited access we have for editing page elements when cloud-hosted. 

* AddMutation calls should generally be placed in your header.html file so that it can apply mutations as the
rest of the page loads. Placing it in the footer would have it miss most of the page content.

* Grouping related selectors into one larger one is better. For example, instead of making a (selector,fn) pair
  for each item in the Main Menu, target the Main Menu.
  
* Among the (selector,fn) tuples, selectors need to be unique. The following is not possible: ('div',function1) and
('div', function2)

* The callback functions you provide should include one parameter as follows:  `function methodName(elem) { ... }`. The 
parameter *elem* will be a DOM node for the specific element found by DOM_WAITER. You can use it to do any manipulations 
and even create a jQuery object from it: `var $this = jQuery(elem);`.

* Mutations are generally applied in the same order as elements are added to the page. If two selectors match the same 
element, their respective callback functions will be applied in the order that the selector/function pairs were added
to the dom_waiter.

* Small improvements in memory can be gained by removing selectors once they become unnecessary. This can be done
by calling the `DOM_WAITER.remove(selector)` method in your callback function. A more efficient approach is to use
the *autoDelete* parameter in the `AddMutation(sel,fn,autoDelete,JQ) method above. By default, *AddMutation* will
automatically delete the selector the first time it matches.

* Similarly to the above item, dom_waiter.js provides a `DOM_WAITER.shutdown()` method to turn off all its monitoring.

## How It Works  
The goal of the DOM_WAITER is to determine when a specified element gets added to the DOM. This is tricky because one
cannot place an event on a DOM element until it gets added. Instead, we have to monitor the whole page document for a 
change and then determine if the element of interest is present. If it is, we run the function we want to on the element
and then mark that element as having been detected. This marking is what prevents us from repeatedly the running the
associated function again. Because an element may be targetted by multiple selectors (which was a design decision), 
an element gets marked with the name of the selector currently targeting it. 

More formally, the DOM_WAITER consists of essentially two components: a Mutation Observer (an event listener for DOM 
mutation events) and a *watch_list* of *(selector,fn)* to trigger on. The observer (*sentry*) is set to monitor the entire 
document for any DOM additions/deletions. Thus, whenever a a mutation event is observed by *sentry* via the
`scan()` method, the following happens:

```
for each (selector,fn) in the watch_list
   elements = list of elements matching the selector
   for each element in elements
      if element's mark equals selector
         // this element was seen before, so don't touch it again
         continue
      else // element is not marked or element's mark != selector
         run fn(element)
         Mark element as being last visited by selector
   end for
end for
```
### How Removing a Selector is Handled  
As suggested under *Usage Notes* earlier, some efficiency can be gained by removing (selector,fn) pairs when they are no 
longer needed. Because DOM maintenance and events is a single-threaded process, this has to be done carefully to 
avoid concurrency problems. The DOM_WAITER maintains a list of selectors to remove: *remove_list*. Each time `scan()` 
runs, a new watch_list is generated. If a selector is in the remove_list, it is both ignored and not added to the new
watch_list. Otherwise, the (selector,fn) pair placed in the new watch_list. By then end of the call to scan, the
remove_list is empty as all indicated selectors have not been added into the new watch_list.
## Acknowledgements  
Insights for creating the DOM_WAITER code came from the 
[ready.js code from ryanmorr](https:/*github.com/ryanmorr/ready). Much thanks goes
out to his shared code.
 
