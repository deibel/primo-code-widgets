# Replace the Search Scope Dropdown with a Select

On the default tab in basic search, the Search Scope dropdown is inaccessible to keyboard interaction:

![Screenshot showing the inaccessible search scope dropdown marked by a purple arrow](./example-original-dropdown.png "Screenshot showing the inaccessible search scope dropdown marked by a purple arrow")

This widget replaces the dropdown with a more accessible, more responsive select input:

![Screenshot showing the inserted select menu marked by a purple arrow](./example-with-select.png "Screenshot showing the inserted select menu marked by a purple arrow")

Given that search scope is part of the search form, it should have originally been a <select> input.

## Technical Specifications

### Primo Code Widgets Dependencies 
[MutationSupport](../MutationSupport/) and [BetterHiddenCues](../BetterHiddenCues) are recommended.

### Browser Compatilbility
Firefox, Chrome, Safari, IE8+, Edge

### Accessibility
Improves keyboard accessibility

### Responsiveness
Improves mobile usability as most devices will use a native user interface when the <select> is focused on.

## Usage

Because of the complexity of how the search scope dropdown works (it actually interacts with a hidden radio set), 
this widget does not actually remove it and replace it with the <select>. Instead, the dropdown is hidden and the 
<select>'s events manipulate the dropdown behind the scenes. 

This widget requires both CSS and JavaScript code.

### Installation

The search scope select code consists of a JavaScript file and a CSS file. Both files should be linked to from your
header.html file in some way. Copying the contents of each file into an already existing JavaScript and CSS file
is recommended.

In the *DefaultSearchScopeSelect.js* file , one of two options must be activated . If you are using
the [MutationSupport](../MutationSupport/) widget, uncomment the `AddMutation` code:

```
AddMutation('#scopesList', replaceBasicDefaultScopesList);
```

Otherwise, uncomment the `jQuery(document).ready` code:

```
// jQuery(document).ready(function() {
    // jQuery('#scopesList').each(function(i,elem) { replaceBasicDefaultScopesListr(elem); });
// });

```

### Customization  
There isn't much to customize here. The contents of the <select> menu are derived from the scope
dropdown. The provided CSS file, *DefaultSearchScopeSelect.css*, includes some CSS code to make
sure that the added <select> lines up well with the rest of the contents of the search form.

### Usage Notes

* There is current a problem with the <select> not being properly initialized when coming into Primo
from a deep link. This is due to an HTML issue on Primo's end. It is not a critical problem; searches
will work just fine. 

* Because different browsers render select inputs differently, the scope <select> will look different
in different browsers. This is largely unavoidable and cannot be addressed with just CSS. 