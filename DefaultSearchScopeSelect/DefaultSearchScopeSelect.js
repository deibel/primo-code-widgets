/* REPLACE THE DEFAULT SEARCH SCOPE DROPDOWN WITH A SELECT MENU
   -----------------------------------------------------------------
   This JavaScript replaces the inaccessible search scope dropdown on
   the default tab of basic search. The select menus works with the 
   dropdown (now hidden) so that Primo functionality is maintained.
   
   Last edited 2016.02.04 by Kate Deibel
*/

/* -------------------------------------- */
/* UNCOMMENT ONE OF THE TWO OPTIONS BELOW */
/* -------------------------------------- */

/* OPTION 1: Uncomment the code below if using the MutationSupport widget */
// AddMutation('#scopesList', replaceBasicDefaultScopesList);

/* OPTION 2: Uncomment the code below if NOT using MutationSupport */
// jQuery(document).ready(function() {
    // jQuery('#scopesList').each(function(i,elem) { replaceBasicDefaultScopesList(elem); });
// });

function replaceBasicDefaultScopesList(elem) {
    /* should only appear but just in case on the library resources (default) tab */
    if( jQuery('#tab').val() == 'default_tab') {
        var scopes = jQuery(elem);
        var selectDiv = jQuery('<div id="localid_searchSelectDiv"></div>');
        selectDiv.append('<label class="EXLHiddenCue" for="localid_scopeSelect">Search in:</label>');
        var select = jQuery('<select id="localid_scopeSelect" name="localid_scopeSelect"></select>');
        /* load values from the primo scopes dropdown */
        scopes.find('#scopesListContainer > div').each( function(i, elem) {
            var $this = jQuery(elem);
            var opt = jQuery('<option></option>');
            opt.attr('value', $this.attr('id'));
            opt.html($this.find('a label').html().trim());

            select.append(opt);
        });
        /* set current value of the new select to primo's */       
        select.val(jQuery('input[name="scp.scps"]:checked').closest('div').attr('id'));
        /* whenever our select registers a change, force a click on the hidden select */
        select.change( function() {
            jQuery('#' + jQuery('#localid_scopeSelect').val()).click();
        });
        selectDiv.append(select);
        selectDiv.insertBefore(scopes);
        scopes.hide();
        
        /* Add a scope check to the form's onsubmit since sometimes, the select change
           event doesn't trigger properly due to an odd timing issue.
         */
        jQuery('#searchForm').attr('onsubmit', 'lastMinuteSearchScopeCheck(); ' + jQuery('#searchForm').attr('onsubmit'));
    }
}

/* makes sure the correct scope radio button has been checked */
function lastMinuteSearchScopeCheck() {
    var scopeDiv = jqPrimo('#' + jqPrimo('#localid_scopeSelect').val());
    
    scopeDiv.click();
    
    /* checks for weird case where the event listeners on the div are not yet 
       activated due to partial page load and searching then */
    if( ! scopeDiv.find('input[type="radio"]')[0].checked ) {
        scopeDiv.find('input[type="radio"]')[0].checked = true;
        /* Normally, we'd want to copy everything that Primo does on this radio change,
           but we're only going to a new page so no worries */
    }
}