# Primo Page Identification Functions

This widget contains several JavaScript functions that may help when writing scripts by
askinging what type of page is currently being shown:

* Are we on a basic search page? Advanced?

* Are we using a specialty search like Browse or the A-Z E-Journal search?

* Are we on a services page?

## Technical Specifications  

### Primo Code Widgets Dependencies
None.

### Browser Compatibility  
Firefox, Chrome, Safari, IE6+, Edge

### Accessibility
Not applicable.

### Responsiveness
Not applicable.

## Usage

### Installation

The *page-id-functions.js* is for the most part ready to use as is. The JavaScript file need only 
be linked to in your header HTML file.

You may need to edit the `onServicesPage()` function depending on what the VID (view ID) of your 
services page is. The code assumes that the VID contains the text *services_age* as this was the 
naming convention suggested to us by Ex Libris. If your VID is different, simply edit the contents
of the text within the `indexOf(...)`  function in the `return` statement of the `onServicesPage()`
function.

### Usage

The various helper functions can be called directly. Each returns `true` or `false` depending on
what type of page you are currently on. Their usage is equivalent to the built-in Primo
`isFullDisplay()` function.

Current functions are:
```
onServicesPage()
onBasicSearch()
onAdvancedSearch()
onBrowseSearch()
onEJournalSearch()
```

### Notes

* Most of the functions rely upon Primo-defined CSS classes or hidden <input> tags. For the most
  part, customizations should not break these functions.
  
* The functions may return false incorrectly if the function is called before the necessary element
  or class is placed into the DOM. This can occur when using the [MutationObserver](../MutationObserver/)
  code. This is not an issue for any of the widgets in this repository, however.




