/* PRIMO HELPER FUNCTIONS
   -----------------------------------------------------------------
   Helper functions for determining what type of results page
   (basic, advanced, browse, A-Z E-Journal, or services page) 
   one is on. These functions are in the same vein as the 
   built-in Primo function isFullDisplay(). 
   
   NOTE: Some may provide false negatives if called before the 
         necessary DOM elements have not been loaded yet.
         
         You may also need to customize the onServicesPage() 
         function if the VID for your services page does not
         match the provided pattern (contains 'services_page').
   
   Last edited 2016.02.11 by Kate Deibel
*/

/* Determine if you are on a Primo services page. This is a test against
   the View ID (VID) of your services page. You may need to customize this
   function based on your view name.
*/
function onServicesPage() {
    var vid = jQuery('#vid').val();
    return ( (vid !== undefined) && (vid.indexOf('services_page') != -1) );
}

/* Determine if on a basic search page */
function onBasicSearch() {
    var val = jQuery('#mode').val();
    return ((val !== undefined) && (val == 'Basic'));
}

/* Determine if on an advanced search page */
function onAdvancedSearch() {
    var val = jQuery('#mode').val();
    return ((val !== undefined) && (val == 'Advanced'));
}

/* Determine if on a browse search page */
function onBrowseSearch() {
    var form = jQuery('#searchForm');
    return ( (form[0] !== undefined) && (form.attr('action').indexOf('BrowseSearch') != -1) );
}

/* Determine if on an A-Z E-Journal page */
function onEJournalSearch() {
    return (jQuery('body').hasClass('EXLAlmaAz'));
}

