# Move the Item Actions Out of the Tabs

In Primo 4, item actions (E-mail, Permalink, etc.) can only be accessed by first opening a tab and then interacting
with the keyboard inaccessible dropdown menu:

![Screenshot showing the default item action menu in an opened tab](./old-actions-screenshot.png "Screenshot showing the default item action menu in an opened tab")

This widget creates a new dropdown menu for the item actions that is placed outside the tabs for easier access. 
Additional item action buttons can be created and placed in the toolbar outside the dropdown for even faster
access.

![Screenshot showing the new item action menu outside of the tabs](./moved-actions-screenshot.png "Screenshot showing the new item action menu outside of the tabs")


## Technical Specifications  

### Primo Code Widgets Dependencies
[ItemActionsLibrary](../ItemActionsLibrary/) and [PrimoDropdownLibrary](../PrimoDropdownLibrary/) are required. 
[BetterHiddenCues](../BetterHiddenCues/) and [MutationSupport](../MutationSupport/) are recommended.

### Browser Compatibility

Firefox, Chrome, Safari, IE8+, Edge

### Accessibility

Significantly improves keyboard accessibility.

### Responsiveness

Equivalent to basic Primo responsiveness.

## Usage

### Installation

This widget is fairly complex and has several immediate and indirect Primo Code Widget Dependencies. 
Be sure to install these dependencies first.

Otherwise, the critical code to this feature is contained within two files: *move-item-actions.js*
and *move-item-actions.css*.  Both files should be linked to from your header html file in some way. 
Copying the contents of each file into an already existing JavaScript and CSS file is recommended.

In the JavaScript file, one of two options must be activated.  If you are using
the [MutationSupport](../MutationSupport/) widget, uncomment the two `AddMutation` function calls:

```
//AddMutation('tr.EXLResult td.EXLSummary', addItemActionsToResult, false); 
//AddMutation('.EXLFullView .EXLResult .EXLTabHeaderButtonSendToList', addItemActionsToResult);
```

Otherwise, uncomment the `jQuery(document).ready(...)` code:
```
// jQuery(document).ready(function() {
    // jQuery('tr.EXLResult td.EXLSummary').each(function(i,elem) { addItemActionsToResult(elem); });
    // jQuery('.EXLFullView .EXLResult .EXLTabHeaderButtonSendToList').each(function(i,elem) { addItemActionsToResult(elem); });
// });
```

Additional lines of code within *move-item-actions.js* may need additional editing for customization purposes.


### Customization

Unfortunately, this code cannot access information within the PBO. This means that the name for the item
actions menu, which actions are included in the menu, and the actions' ordering cannot be gathered
programmatically. You must configure these aspects within the *move-item-actions.js* file itself.  The 
sections that you can edit to make these changes are commented within *move-item-actions.js*

#### Configuring the Menu Name

The dropdown menu's name and hovertext is configured within the `generateToolbar(...)` function via
two variables: `buttonText` and `buttonTitle`. Just change the text within the apostrophes to customize
the menu to your specifications.

#### Configuring What Actions Appear and Their Ordering

Determining which actions appear and their order is configured within the ` generateItemToolDropdown(...)`
function. Each item is added to the menu by a line of code like:
```
dropdown.AddButtonItem(toolsGen.EasyBib());
```
This code uses the [ItemActionsLibrary](../ItemActionsLibrary/) object `toolsGen` to create an item action
`<button>` into the `dropdown` object. If you have created a custom item action, you need to place the
button within the menu using the same `dropdown.AddButtonItem()` function.

Items appear in the menu in the order they are added to the dropdown.

#### Adding Item Actions Outside the Menu

As show in the second screenshot above, a Permalink and Citation button were placed outside the dropdown menu
for easier access (they also appear inside). There is support code for doing this within the `generateToolbar(...)` 
function. Example code is provided (and commented out):
```
/********************************************************
 Add a tool button to the toolbar (not in the dropdown)
 ********************************************************/
//toolbar.append( generateInlineItemToolButton(toolsGen.eShelf(), index) );
```

#### Look and Feel

Most of the CSS for the moved item action menu is actually configured within the CSS for two other widgets
[ItemActionsLibrary](../ItemActionsLibrary/) and [PrimoDropdownLibrary](../PrimoDropdownLibrary/). However,
if you wish to customize the item action dropdown separately from other dropdowns you can use the 
`.localItemToolsMenu` CSS class to target them specifically.

## Additional Notes
* Remember that the item actions' text, icons, colors, etc. are configured in the code for the 
[ItemActionsLibrary](../ItemActionsLibrary/) widget.

* The CSS file *move-item-actions.css* includes a rule to hide the within tabs menu: 
`div.EXLTabHeader .EXLTabHeaderButtons .EXLTabHeaderButtonSendTo`.

* If you still use the default e-Shelf star column on brief results, the code automatically keeps everything
in sync. If you wish to replace the default e-Shelf button as we do at the [UW libraries](http://search.lib.uw.edu/)
by placing a button within the added toolbar, you should add the following CSS to hide the e-Shelf column on brief
results:  
  
    ``` .EXLResultsList th.EXLMyShelfStar, .EXLResultsList td.EXLMyShelfStar { display: none; }```

    This hides both the visible table cells and the hidden table header cell used by screen readers for the e-Shelf
button.

* This widget places the item actio menu in a different DOM location than the University of Washington's
  site does. This is because we have done further extensive changes to our CSS to enables us to place 
  the toolbar within the `td.EXLMyShelfStar` table cell on brief results. Sharing that code is impractical
  as it induces multiple side effects with page layout.
